# docker/swift-client

It is an image including swift-client based on alpine linux.

# Usage

```sh
docker run --rm -it \
  -v /path/to/dir:/data \
  -e OS_AUTH_URL='http://identity.example.com/v2.0' \
  -e OS_TENANT_NAME='tenant_name' \
  -e OS_USERNAME='username' \
  -e OS_PASSWORD='password' \
  surface0/swift-client
```

# Python Swift Client

[openstack/python\-swiftclient: OpenStack Storage \(Swift\) Client](https://github.com/openstack/python-swiftclient)